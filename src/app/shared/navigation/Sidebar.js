import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Collapse } from 'react-bootstrap';
import { Dropdown } from 'react-bootstrap';

class Sidebar extends Component {
  state = {};

  toggleMenuState(menuState) {
    if (this.state[menuState]) {
      this.setState({ [menuState]: false });
    } else if (Object.keys(this.state).length === 0) {
      this.setState({ [menuState]: true });
    } else {
      Object.keys(this.state).forEach(i => {
        this.setState({ [i]: false });
      });
      this.setState({ [menuState]: true });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged();
    }
  }

  onRouteChanged() {
    document.querySelector('#sidebar').classList.remove('active');
    Object.keys(this.state).forEach(i => {
      this.setState({ [i]: false });
    });

    const dropdownPaths = [
      { path: '/basic-ui', state: 'basicUiMenuOpen' },
      { path: '/form-elements', state: 'formElementsMenuOpen' },
      { path: '/tables', state: 'tablesMenuOpen' },
      { path: '/icons', state: 'iconsMenuOpen' },
      { path: '/charts', state: 'chartsMenuOpen' },
      { path: '/user-pages', state: 'userPagesMenuOpen' },
    ];

    dropdownPaths.forEach((obj => {
      if (this.isPathActive(obj.path)) {
        this.setState({ [obj.state]: true })
      }
    }));

  }
  render() {
    let picture = localStorage.getItem("picture") != null ? localStorage.getItem("picture") : "";
    let name = localStorage.getItem("name") != null ? localStorage.getItem("name") : "";
    return (
      <nav className="sidebar sidebar-offcanvas" id="sidebar">
        <div className="text-center sidebar-brand-wrapper d-flex align-items-center">
          <a className="sidebar-brand brand-logo" href="index.html"><img src={require("../../../assets/logo/logo.png")} alt="logo" /></a>
          <a className="sidebar-brand brand-logo-mini pt-3" href="index.html"><img src={require("../../../assets/logo/logo-mini.png")} alt="logo" /></a>
        </div>
        <ul className="nav">
          <li className="nav-item nav-profile not-navigation-link">
            <div className="nav-link">
                  <div className="d-flex justify-content-between align-items-start">
                    <div className="profile-image">
                      <img src={picture} alt="profile" />
                    </div>
                    <div className="text-left ml-3">
                      <p className="profile-name">{name}</p>
                      <small className="designation text-muted text-small">Developer</small>
                      <span className="status-indicator online"></span>
                    </div>
                  </div>
            </div>
          </li>
          <li className={this.isPathActive('/dashboard') ? 'nav-item active' : 'nav-item'}>
            <Link className="nav-link" to="/dashboard">
              <i className="mdi mdi-television menu-icon"></i>
              <span className="menu-title">Dashboard</span>
            </Link>
          </li>
          <li className={this.isPathActive('/students') ? 'nav-item active' : 'nav-item'}>
            <Link className="nav-link" to="/students">
              <i className="mdi mdi-account-multiple menu-icon"></i>
              <span className="menu-title">Students</span>
            </Link>
          </li>
          <li className={this.isPathActive('/teachers') ? 'nav-item active' : 'nav-item'}>
            <Link className="nav-link" to="/teachers">
              <i className="mdi mdi-television menu-icon"></i>
              <span className="menu-title">Teachers</span>
            </Link>
          </li>
          <li className={this.isPathActive('/class') ? 'nav-item active' : 'nav-item'}>
            <Link className="nav-link" to="/class">
              <i className="mdi mdi-school menu-icon"></i>
              <span className="menu-title">Class</span>
            </Link>
          </li>

          <li className={this.isPathActive('/events') ? 'nav-item active' : 'nav-item'}>
            <Link className="nav-link" to="/events">
              <i className="mdi mdi-television menu-icon"></i>
              <span className="menu-title">Events</span>
            </Link>
          </li>

          <li className={this.isPathActive('/fees') ? 'nav-item active' : 'nav-item'}>
            <Link className="nav-link" to="/fees ">
              <i className="mdi mdi-television menu-icon"></i>
              <span className="menu-title">Fees</span>
            </Link>
          </li>
        </ul>
      </nav>
    );
  }

  isPathActive(path) {
    return this.props.location.pathname.startsWith(path);
  }

  componentDidMount() {
    this.onRouteChanged();
    // add className 'hover-open' to sidebar navitem while hover in sidebar-icon-only menu
    const body = document.querySelector('body');
    document.querySelectorAll('.sidebar .nav-item').forEach((el) => {

      el.addEventListener('mouseover', function () {
        if (body.classList.contains('sidebar-icon-only')) {
          el.classList.add('hover-open');
        }
      });
      el.addEventListener('mouseout', function () {
        if (body.classList.contains('sidebar-icon-only')) {
          el.classList.remove('hover-open');
        }
      });
    });
  }

}

export default withRouter(Sidebar);