import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import { Form } from 'react-bootstrap';
import { Base64 } from 'js-base64';
import FacebookLogin from 'react-facebook-login';
import axios from 'axios';


export class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      submitted: false,
      loading: false,
      error: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  responseFacebook = (response) => {
    if (response.accessToken) {
      this.state.submitted = true;
      localStorage.setItem("jwtToken", response.accessToken);
      localStorage.setItem("picture", response.picture.data.url);
      localStorage.setItem("name", response.name);   
      this.props.history.push("/dashboard");
    } else {
    }
  }
  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault()
    var hash = Base64.encode(this.state);
    this.login(this.state.username, hash);
  
  }

  login(username, password) {
    const requestOptions = { userName: username, password: password };

    try {
      axios.post('/auth/signin', requestOptions).then(response => {
        if (response) {
          const jwtToken = response.data.jwt;
          localStorage.setItem("jwtToken", jwtToken);
          this.props.history.push("/dashboard");
        }
        console.log('👉 Returned data:', response);
      })

    } catch (e) {
      console.log(`😱 Axios request failed: ${e}`);
    }
  };


  render() {
    return (
      <div>
        <div className="d-flex align-items-center auth px-0">
          <div className="row w-100 mx-0">
            <div className="col-lg-4 mx-auto">
              <div className="auth-form-light text-left py-5 px-4 px-sm-5">
                <div className="brand-logo">
                  <img src={require("../../../assets/logo/logo.png")} alt="logo" />
                </div>
                <h4>Hello! let's get started</h4>
                <h6 className="font-weight-light">Sign in to continue.</h6>
                <Form className="pt-3" onSubmit={this.handleSubmit}>
                  <Form.Group className="d-flex search-field">
                    <Form.Control name="username" type="email" placeholder="Username" size="lg" className="h-auto" onChange={this.handleChange} value={this.state.username} />
                  </Form.Group>
                  <Form.Group className="d-flex search-field">
                    <Form.Control name="password" type="password" placeholder="Password" size="lg" className="h-auto" onChange={this.handleChange} value={this.state.password} />
                  </Form.Group>
                  <div className="mt-3">
                    <button type="submit" className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" to="/dashboard">SIGN IN</button>
                  </div>
                  <div className="my-2 d-flex justify-content-between align-items-center">
                    <div className="form-check">
                      <label className="form-check-label text-muted">
                        <input type="checkbox" className="form-check-input" />
                        <i className="input-helper"></i>
                        Keep me signed in
                      </label>
                    </div>
                    <a href="!#" onClick={event => event.preventDefault()} className="auth-link text-black">Forgot password?</a>
                  </div>
                  <div className="mb-2">
                    <FacebookLogin
                      appId="2841455406130961"
                      autoLoad={false}
                      fields="name,email,picture"
                      scope="public_profile,user_friends"
                      callback={this.responseFacebook}
                      icon="fa-facebook"
                      redirectUri="/dashboard"
                      cssClass="btn btn-block btn-facebook auth-form-btn" />
                  </div>
                  <div className="text-center mt-4 font-weight-light">
                    Don't have an account? <Link to="/session/register" className="text-primary">Create</Link>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div >
    )
  }
}

export default Login
