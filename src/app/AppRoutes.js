import React, { Component, Suspense, lazy } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Spinner from '../app/shared/Spinner';
const Login = lazy(() => import('./shared/session/Login'));
const Dashboard = lazy(() => import('./dashboard/Dashboard'));
const Error404 = lazy(() => import('./shared/session/Error404'));
const Error500 = lazy(() => import('./shared/session/Error500'));
const Register = lazy(() => import('./shared/session/Register'));



// const SecretRoute = ({ component: Component, ...rest }) => (
//   <Route {...rest} render={(props) => (
//     AuthService.isAuthenticated === true
//       ? <Component {...props} />
//       : <Redirect to='/login' />
//   )} />
// );
//

// const Buttons = lazy(() => import('./basic-ui/Buttons'));
// const Dropdowns = lazy(() => import('./basic-ui/Dropdowns'));
// const Typography = lazy(() => import('./basic-ui/Typography'));

// const BasicElements = lazy(() => import('./form-elements/BasicElements'));

// const BasicTable = lazy(() => import('./tables/BasicTable'));

// const FontAwesome = lazy(() => import('./icons/FontAwesome'));


// const ChartJs = lazy(() => import('./charts/ChartJs'));

// 
// 

// 
// 

// const BlankPage = lazy(() => import('./user-pages/BlankPage'));


class AppRoutes extends Component {
//isAuthorise = localStorage.getItem("jwtToken") !== null;

  // if (isAuthorise) {
  //   return <Redirect to="/session/login" />
  // }
  render() {
    let isAuthorise = localStorage.getItem("jwtToken") !== null;
    if(isAuthorise){
      return <Redirect to="/dashboard" />
    }
    return (
      <Suspense fallback={<Spinner />}>
        <Switch>
          <Route exact path="/dashboard" component={ Dashboard } />
          <Route path="/session/login" component={ Login } />
          <Route path="/session/register" component={ Register } />
          <Route path="/session/error-404" component={ Error404 } />
          <Route path="/session/error-500" component={ Error500 } />
          <Redirect to="/session/login" />
        </Switch>
      </Suspense>
    );
  }
}

export default AppRoutes;